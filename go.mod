module gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/v2

require (
	github.com/urfave/cli v1.22.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.9.1
	gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan v1.2.0
)

go 1.13
