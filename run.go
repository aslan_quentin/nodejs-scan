package main

import (
	"bufio"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
	"gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/plugin"
)

const (
	artifactName    = "gl-sast-report.json"
	flagTargetDir   = "target-dir"
	flagArtifactDir = "artifact-dir"
	flagRulesPath   = "rules"

	pathHome     = "/home/node"
	pathBabelCli = "./node_modules/.bin/babel"

	// Babel 7 doesn't preserve file paths anymore.
	// As a workaround, we leverage the `--relative` param which
	// creates a folder relatively to the file compiled.
	//
	// Example: ./example/index.js will be compiled to ./example/out/index.js
	// if the output dir is set to "out",
	//
	// That's why we need a name with a low probability of collision
	// with files and folders of the project.
	pathOutDir = "./GitLab-SAST-NodeJsScan-out"

	scannerID   = "nodejs_scan"
	scannerName = "NodeJsScan"
)

// Run is inspired from command.Run of the analyzers/common/v2 library.
func Run() cli.Command {
	flags := []cli.Flag{
		cli.StringFlag{
			Name:   flagTargetDir,
			Usage:  "Target directory",
			EnvVar: "ANALYZER_TARGET_DIR,CI_PROJECT_DIR",
		},
		cli.StringFlag{
			Name:   flagArtifactDir,
			Usage:  "Artifact directory",
			EnvVar: "ANALYZER_ARTIFACT_DIR,CI_PROJECT_DIR",
		},
		cli.StringFlag{
			Name:   flagRulesPath,
			Usage:  "Path of NodeJsScan XMLrules",
			EnvVar: "NODEJS_SCAN_RULES_FILE",
			Value:  filepath.Join(pathHome, "rules.xml"),
		},
	}
	flags = append(flags, cacert.NewFlags()...)
	flags = append(flags, search.NewFlags()...)

	return cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a SAST artifact",
		Flags:   flags,
		Action: func(c *cli.Context) error {
			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errInvalidArgs
			}

			// import CA bundle
			if err := cacert.Import(c, cacert.ImportOptions{}); err != nil {
				return err
			}

			// search directory
			root, err := filepath.Abs(c.String(flagTargetDir))
			root = strings.TrimSuffix(root, "/") + "/"
			if err != nil {
				return err
			}

			// search
			searchOpts := search.NewOptions(c)
			matchPath, err := search.New(plugin.Match, searchOpts).Run(root)
			if err != nil {
				return err
			}
			fmt.Fprintln(c.App.Writer, "Found project in "+matchPath)

			// load rules
			pathRules := c.String(flagRulesPath)
			rules, err := loadRules(pathRules)
			if err != nil {
				return err
			}
			fmt.Fprintln(c.App.Writer, len(rules), "rules loaded") // DEBUG

			// build files list
			files := []string{}

			// save directories where these files are present to clean-up babel compiled files later
			// note: babel creates an empty dir at the root of the project, surely another bug there
			dirs := map[string]struct{}{root: struct{}{}}

			err = filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if info.IsDir() {
					switch info.Name() {
					case "node_modules", "vendor", "spec", "test":
						return filepath.SkipDir
					default:
						// OK
					}
				} else {
					if ok, _ := plugin.Match(path, info); ok {
						files = append(files, path)
						dirs[filepath.Dir(path)] = struct{}{}
					}
				}
				return nil
			})
			if err != nil {
				return err
			}

			// remove comments using Babel
			args := []string{
				"--no-comments",
				"--retain-lines",
				"--no-babelrc",
				"--config-file", filepath.Join(pathHome, "babel.config.json"),
				"--relative",
				"-d", pathOutDir,
			}
			args = append(args, files...)

			cmd := exec.Command(filepath.Join(pathHome, pathBabelCli), args...)
			cmd.Env = os.Environ()
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			if err := cmd.Run(); err != nil {
				return err
			}

			// apply rules to find issues
			issues := []issue.Issue{}
			for _, f := range files {
				compiledFilePath := filepath.Join(filepath.Dir(f), pathOutDir, filepath.Base(f))
				issue, err := scanFile(compiledFilePath, strings.Replace(f, root, "", 1), rules)
				if err != nil {
					return err
				}
				issues = append(issues, issue...)
			}

			// cleanup all files
			for d := range dirs {
				err := os.RemoveAll(filepath.Join(d, pathOutDir))
				if err != nil {
					return err
				}
			}

			// generate report artifact
			report := issue.NewReport()
			report.Vulnerabilities = issues

			artifactPath := filepath.Join(c.String(flagArtifactDir), artifactName)
			f, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer f.Close()
			enc := json.NewEncoder(f)
			enc.SetIndent("", "  ")
			return enc.Encode(report)
		},
	}
}

func loadRules(path string) ([]Rule, error) {
	set := RuleSet{}
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	err = xml.NewDecoder(f).Decode(&set)
	return set.Rules()
}

func scanFile(path, realPath string, rules []Rule) ([]issue.Issue, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	var lineNumber int
	var issues []issue.Issue
	var issueScanner = issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}
	for scanner.Scan() {
		lineNumber++
		line := scanner.Text()
		for _, rule := range rules {
			if rule.Match(line) {
				def := rule.Definition()
				issues = append(issues, issue.Issue{
					Category:    issue.CategorySast,
					Scanner:     issueScanner,
					Name:        def.Name,
					Message:     def.Name,
					Description: def.Description,
					Location: issue.Location{
						File:      realPath,
						LineStart: lineNumber,
					},
					CompareKey: def.Name + ":" + line,
					Identifiers: []issue.Identifier{
						def.NodeJsScanIdentifier(),
					},
				})
			}
		}
	}
	return issues, err
}
