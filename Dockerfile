FROM  node:9.11.2-alpine

ARG NODEJS_SCAN_VERSION
ARG RULES_SHA256SUM=35d8493184fe82cbb00d52e02d63b74a80d4ca1237e8056217dfb1eff2faeb59

# Please read the "Updating the underlying Scanner" section in
# the README before bumping to a newer version
ENV NODEJS_SCAN_VERSION=${NODEJS_SCAN_VERSION:-3.7} \
    NODE_PATH=/home/node/node_modules

# The node user below doesn't have permission to create a file in /etc/ssl/certs, this
# RUN command creates a file that the analyzer can add additional ca certs to trust.
RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chown root:node /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chmod g+w /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem

USER node
WORKDIR /home/node
COPY --chown=node:node babel.config.json .
COPY --chown=node:node package.json .
COPY --chown=node:node yarn.lock .

RUN yarn --frozen-lockfile && yarn cache clean

ADD --chown=node:node https://raw.githubusercontent.com/ajinabraham/NodeJsScan/v${NODEJS_SCAN_VERSION}/core/rules.xml .

RUN echo "$RULES_SHA256SUM  rules.xml" | sha256sum -c

COPY --chown=root:root analyzer /

ENTRYPOINT []
CMD ["/analyzer", "run"]
