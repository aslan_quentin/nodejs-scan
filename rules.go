package main

import (
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

// RuleSet is a set of rules.
type RuleSet struct {
	StringRuleDefs []RuleDef `xml:"rule"`
	RegexRuleDefs  []RuleDef `xml:"regex"`
}

// RuleDef is a rule definition.
type RuleDef struct {
	Name        string `xml:"name,attr"`
	Signature   string `xml:"signature"`
	Description string `xml:"description"`
	Tag         string `xml:"tag"`
}

// Rule is implemented by a rule so that it can be evaluated.
type Rule interface {
	Match(string) bool
	Definition() RuleDef
}

// StringRule uses string comparison to establish whether a line contains vulnerable code.
type StringRule RuleDef

// Match tells whether the rule applies to the given line of the source code.
func (r StringRule) Match(s string) bool {
	return strings.Contains(s, r.Signature)
}

// Definition returns the definition of the rule.
func (r StringRule) Definition() RuleDef {
	return RuleDef(r)
}

// NewStringRule creates a new string-based rule.
func NewStringRule(def RuleDef) StringRule {
	return StringRule(def)
}

// RegexRule uses a regexp to establish whether a line contains vulnerable code.
type RegexRule struct {
	RuleDef
	*regexp.Regexp
}

// Match tells whether the rule applies to the given line of the source code.
func (r RegexRule) Match(s string) bool {
	return r.Regexp.MatchString(s)
}

// Definition returns the definition of the rule.
func (r RegexRule) Definition() RuleDef {
	return r.RuleDef
}

// NewRegexRule creates a new regex-based rule.
func NewRegexRule(def RuleDef) (*RegexRule, error) {
	sig := def.Signature
	sig = strings.Replace(sig, "{0,4000}", "{0,100}", -1)  // HACK: work around invalid repeat count
	sig = strings.Replace(sig, "{0,40000}", "{0,100}", -1) // HACK: work around invalid repeat count
	r, err := regexp.Compile(sig)
	if err != nil {
		return nil, err
	}
	return &RegexRule{def, r}, nil
}

// Rules converts StringRules and RegexRules to a slice of Rule.
func (set RuleSet) Rules() ([]Rule, error) {
	rules := []Rule{}
	for _, def := range set.StringRuleDefs {
		if def.RuleID() == "" {
			return nil, fmt.Errorf("No Identifier found for: %s", def.Name)
		}
		rules = append(rules, NewStringRule(def))
	}
	for _, def := range set.RegexRuleDefs {
		if def.RuleID() == "" {
			return nil, fmt.Errorf("No Identifier found for: %s", def.Name)
		}
		rule, err := NewRegexRule(def)
		if err != nil {
			return nil, err
		}
		rules = append(rules, rule)
	}
	return rules, nil
}

// NodeJsScanIdentifier returns a structured Identifier for a NodeJs Scan rule
func (def RuleDef) NodeJsScanIdentifier() issue.Identifier {
	return issue.Identifier{
		Type:  "node_js_scan_id",
		Name:  fmt.Sprintf("NodeJsScan ID: %s", def.RuleID()),
		Value: def.RuleID(),
	}
}

// RuleID returns a unique ID for the given rule.
func (def RuleDef) RuleID() string {
	return rulesIdentifiers[def.Name]
}

// rulesIdentifiers is a map based on NodeJS Scan rules to generate a unique identifier.
// This needs to be updated when bumping NodeJS Scan and new rules are addeed.
var rulesIdentifiers = map[string]string{
	"Express BodyParser Tempfile Creation Issue":                    "1",
	"Handlebars Unescaped String":                                   "2",
	"Server Side Injection(SSI) - eval()":                           "3",
	"Server Side Injection(SSI) - setTimeout()":                     "4",
	"Server Side Injection(SSI) -setInterval()":                     "5",
	"Server Side Injection(SSI) - new Function()":                   "6",
	"Deserialization Remote Code Injection":                         "7",
	"Loading of untrusted YAML can cause Remote Code Injection":     "8",
	"Accept Self Signed Certificates":                               "9",
	"Escaping is Disabled":                                          "10",
	"node-curl SSL Verification is Disabled":                        "11",
	"Weak Hash used - MD5":                                          "12",
	"Weak Hash used - SHA1":                                         "13",
	"Password Hardcoded":                                            "14",
	"Secret Hardcoded":                                              "15",
	"Username Hardcoded":                                            "16",
	"XSS Filter is turned off":                                      "17",
	"Directory Traversal - (createReadStream())":                    "18",
	"Directory Traversal - (readFile())":                            "19",
	"Open Redirect":                                                 "20",
	"SQLi - SQL Injection":                                          "21",
	"NoSQLi - NoSQL JavaScript Injection":                           "22",
	"XSS - Reflected Cross Site Scripting":                          "23",
	"HTTP Header Injection Vulnerability":                           "24",
	"Unescaped variable in Mustache.js/Handlebars.js template file": "25",
	"Unescaped variable in Dust.js template file":                   "26",
	"Unescaped variable in Pug.js template file":                    "27",
	"Unescaped variable in EJS template file":                       "28",
	"Unescaped variable in ECT template file":                       "29",
	"Remote OS Command Execution - (child_process.exec())":          "30",
	"SSRF - Server Side Request Forgery - request()":                "31",
	"SSRF - Server Side Request Forgery - request.get()":            "32",
	"SSRF - Server Side Request Forgery - needle()":                 "33",
	"Missing Security Header - Content-Security-Policy (CSP)":       "34",
	"Missing Security Header - X-Frame-Options (XFO)":               "35",
	"Missing Security Header - Strict-Transport-Security (HSTS)":    "36",
	"Missing Security Header - Public-Key-Pins (HPKP)":              "37",
	"Missing Security Header - X-XSS-Protection:1":                  "38",
	"Missing Security Header - X-Content-Type-Options":              "39",
	"Missing Security Header - X-Download-Options: noopen":          "40",
	"Missing 'httpOnly' in Cookie":                                  "41",
	"Information Disclosure - X-Powered-By":                         "42",
	"Use Strict":                                                    "43",
}
